#ifndef _LIBMANDA_LIBMANDA_PROTOCOL_H
#define _LIBMANDA_LIBMANDA_PROTOCOL_H

/*
 * The webserver side is considered to be the *client* here,
 * and the spawn manager the *server*.
 *
 *     16-bit   |   16-bit
 * +------------+------------+
 * |  Command   | Paket size |
 * +-------------------------+
 * | Req ID     | ResponseID |
 * +-------------------------+
 * | Payload...              |
 * +-------------------------+
 *
 * ReqID == 0 if no response is expected
 * ResponseID != 0: response to the request with that id
 * Each side maintains its own set of request ids
 *
 * A response may use a different command; this depends on the commands.
 *
 * A request id can be reused after a response for it was received.
 * Don't use the same id for different commands at the same time.
 *
 * For now, commands are either:
 *  - Requests: they have ReqID != 0 and ResponseID == 0
 *  - Responses: ReqID == 0 and ResponseID == the request id the response is for
 *  - Notifications: ReqID == 0 and ResponseID == 0
 *
 * Encoding rules:
 *
 *   Integers are encoded in network byte order: the word 0xABCD is encoded as 0xAB 0xCD
 *
 *   Socket adresses are encoded as strings, currently the following formats are defined:
 *    - unix:/path/to/socket
 *    - tcp:127.0.0.1:9000
 *    - tcp:[::1]:9000
 *    - udp:127.0.0.1:9000
 *    - udp:[::1]:9000
 *
 *   Strings are encoded as | length (unsigned 16-bit) | length * byte |, without terminating '\0'
 *
 * Commands:
 *
 * - 0x0001: Bind a new backend
 *   Client -> Server (Request): bind_backend
 *     payload is the "name" string.
 *
 *     request the socket address for a "new" backend; the name can
 *     basically be anything, it is recommended to use either the
 *     socket address (for example "unix:/var/run/fastcgi_php_www-default.sock")
 *     or a comma separated list of key=value pairs ("type=php5,user=www-default")
 *
 *   Server -> Client (Response): return_backend
 *     payload is a 32-bit backend identifier, followed by the socket address (or an error message)
 *
 *     A valid backend identifier mustn't be zero, and a valid backend must have a valid socket address.
 *     An error is signaled by a zero identifier, the following string contains an optional error message.
 *
 *     The server should not return the same id more than once, not even for the same backend;
 *     but different ids can point to the same backend and address.
 *     (The reason here is that the server needs to sum up the update notifications.)
 *     The backend id should only be unique per connection; you can reuse the ids for a different
 *     connection.
 *
 * - 0x0002: Release backend
 *   Client -> Server (Notification): release_backend
 *     payload is the 32-bit backend identifier
 *
 *     A connection close is an implicit release of all remaining backends.
 *
 *   Server -> Client (Notification): lost_backend
 *     payload ist the 32-bit backend identifier, followed by a string describing the reason
 *
 *     I the client didn't already release the backend, it should send a release_backend
 *     notification to release the id.
 *
 * - 0x0003: Update backend
 *   Client -> Server (Notification): update_backend
 *     payload are three unsigned 32-bit integers: backend identifier, "load" and "workers"
 *     "load" is the number of requests the client would like to process with the backend right now,
 *     and "workers" is the number of "workers" the client thinks the server should spawn.
 *
 *     It is up to the server what to make of this information; in most cases it will ignore the "workers"
 *     parameter and calculate the needed workers from the "load" parameter.
 *
 *     The "workers" parameter may be useful in cases where you cascade different spawn-managers; the
 *     main spawn-manager may tell a sub spawn-manager how many children / threads it should use.
 *
 * - 0xffff: Unknown command
 *   Any direction; by default a notification, can be a response.
 *     payload is the 16-bit id of the unknown command.
 *
 *     If you get such a message, you should check for the id of the command, and close the connection
 *     if the original message wasn't optional.
 *
 */

typedef enum {
	MANDA_CMD_BIND_BACKEND              = 0x0001,
	MANDA_CMD_RELEASE_BACKEND           = 0x0002,
	MANDA_CMD_UPDATE_BACKEND            = 0x0003,

	MANDA_CMD_UNKNOWN_COMMAND           = 0xffff
} manda_commands;

#endif
